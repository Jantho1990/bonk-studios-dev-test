<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\Post;
use App\User;

/**
 * Handles logic for the homepage.
 * @package App\Http\Controllers
 */
class HomeController extends Controller
{

    public function testHome()
    {
        $posts = Post::orderBy('created_at', 'desc')->get();
        return view('home', compact('posts'));
    }

    public function add()
    {
        $users = User::all();
        return view('add', compact('users'));
    }

    public function addPost(Request $request)
    {
        $data = $request->validate([
            'title' => 'required|min:5',
            'body' => 'required|min:5',
            'user_id' => 'required|integer|exists:users,id'
        ]);

        Post::create($data);

        return 'Success';
    }

    public function edit(Post $post)
    {
        $users = User::all();
        return view('edit', compact('post', 'users'));
    }

    public function editPost(Request $request, Post $post)
    {
        $data = $request->validate([
            'id' => 'required|integer|exists:posts,id',
            'title' => 'required|min:5',
            'body' => 'required|min:5',
            'user_id' => 'required|integer|exists:users,id'
        ]);

        $post->update($data);

        return 'Success';
    }

}
