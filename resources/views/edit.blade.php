<!DOCTYPE html>
<html lang="en">
<head>
    @include('partials._head')
</head>

<body>

@include('partials._nav')

<div class="container">
    @yield('content')
    <div class="errors">
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
    </div>
    <div id="app">
        <ajax-form
            _title="{{ $post->title }}"
            _body="{{ $post->body }}"
            _user_id="{{ $post->user_id }}"
            _post_id="{{ $post->id }}"
            :_users="{{ json_encode($users) }}"
        ></ajax-form>
    </div>
    @include('partials._footer')

</div> <!-- end of .container -->

@include('partials._javascript')
<script type="application/javascript">

</script>

@yield('scripts')

</body>
</html>